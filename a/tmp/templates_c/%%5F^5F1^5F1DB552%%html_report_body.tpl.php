<?php /* Smarty version 2.6.26, created on 2011-07-29 13:23:55
         compiled from /var/www/a/plugins/CoreHome/templates/html_report_body.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', '/var/www/a/plugins/CoreHome/templates/html_report_body.tpl', 3, false),array('modifier', 'translate', '/var/www/a/plugins/CoreHome/templates/html_report_body.tpl', 7, false),array('function', 'cycle', '/var/www/a/plugins/CoreHome/templates/html_report_body.tpl', 25, false),)), $this); ?>
<a name ="<?php echo $this->_tpl_vars['reportId']; ?>
"/>
<h2 style="color: rgb(<?php echo $this->_tpl_vars['reportTitleTextColor']; ?>
); font-size: <?php echo $this->_tpl_vars['reportTitleTextSize']; ?>
pt;">
	<?php echo ((is_array($_tmp=$this->_tpl_vars['reportName'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>

</h2>

<?php if (empty ( $this->_tpl_vars['reportRows'] )): ?>
	<?php echo ((is_array($_tmp='CoreHome_ThereIsNoDataForThisReport')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>

<?php else: ?>
	<table style="border-collapse:collapse; margin-left: 5px">
		<thead style="background-color: rgb(<?php echo $this->_tpl_vars['tableHeaderBgColor']; ?>
); color: rgb(<?php echo $this->_tpl_vars['tableHeaderTextColor']; ?>
); font-size: <?php echo $this->_tpl_vars['reportTableHeaderTextSize']; ?>
pt;">
			<?php $_from = $this->_tpl_vars['reportColumns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['columnName']):
?>
			<th style="padding: 6px 0px;">
				&nbsp;<?php echo $this->_tpl_vars['columnName']; ?>
&nbsp;&nbsp;
			</th>
			<?php endforeach; endif; unset($_from); ?>
		</thead>
		<tbody>
			<?php $_from = $this->_tpl_vars['reportRows']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowId'] => $this->_tpl_vars['row']):
?>

			<?php $this->assign('rowMetrics', $this->_tpl_vars['row']->getColumns()); ?>
			<?php if (isset ( $this->_tpl_vars['reportRowsMetadata'][$this->_tpl_vars['rowId']] )): ?>
				<?php $this->assign('rowMetadata', $this->_tpl_vars['reportRowsMetadata'][$this->_tpl_vars['rowId']]->getColumns()); ?>
			<?php endif; ?>

			<tr style="<?php echo smarty_function_cycle(array('delimiter' => ';','values' => ";background-color: rgb(".($this->_tpl_vars['tableBgColor']).")"), $this);?>
">
				<?php $_from = $this->_tpl_vars['reportColumns']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['columnId'] => $this->_tpl_vars['columnName']):
?>
				<td style="font-size: <?php echo $this->_tpl_vars['reportTableRowTextSize']; ?>
pt; border-bottom: 1px solid rgb(<?php echo $this->_tpl_vars['tableCellBorderColor']; ?>
); padding: 5px 0px 5px 5px;">
					<?php if ($this->_tpl_vars['columnId'] == 'label'): ?>
						<?php if (isset ( $this->_tpl_vars['rowMetrics'][$this->_tpl_vars['columnId']] )): ?>
							<?php if (isset ( $this->_tpl_vars['rowMetadata'][$this->_tpl_vars['rowId']]['logo'] )): ?>
							<img src='<?php echo $this->_tpl_vars['currentPath']; ?>
<?php echo $this->_tpl_vars['rowMetadata'][$this->_tpl_vars['rowId']]['logo']; ?>
'>&nbsp;
							<?php endif; ?>
							<?php if (isset ( $this->_tpl_vars['rowMetadata'][$this->_tpl_vars['rowId']]['url'] )): ?>
								<a style="color: rgb(<?php echo $this->_tpl_vars['reportTextColor']; ?>
);" href='<?php echo ((is_array($_tmp=$this->_tpl_vars['rowMetadata'][$this->_tpl_vars['rowId']]['url'])) ? $this->_run_mod_handler('escape', true, $_tmp, 'url') : smarty_modifier_escape($_tmp, 'url')); ?>
'>
							<?php endif; ?>
									<?php echo $this->_tpl_vars['rowMetrics'][$this->_tpl_vars['columnId']]; ?>

							<?php if (isset ( $this->_tpl_vars['rowMetadata'][$this->_tpl_vars['rowId']]['url'] )): ?>
								</a>
							<?php endif; ?>
						<?php endif; ?>
					<?php else: ?>
						<?php if (empty ( $this->_tpl_vars['rowMetrics'][$this->_tpl_vars['columnId']] )): ?>
							0
						<?php else: ?>
							<?php echo $this->_tpl_vars['rowMetrics'][$this->_tpl_vars['columnId']]; ?>

						<?php endif; ?>
					<?php endif; ?>
				</td>
				<?php endforeach; endif; unset($_from); ?>
			</tr>
			<?php endforeach; endif; unset($_from); ?>
		</tbody>
	</table>
<?php endif; ?>
<br/>
<a style="text-decoration:none; color: rgb(<?php echo $this->_tpl_vars['reportTitleTextColor']; ?>
); font-size: <?php echo $this->_tpl_vars['reportBackToTopTextSize']; ?>
pt" href="#reportTop">
	<?php echo ((is_array($_tmp='PDFReports_TopOfReport')) ? $this->_run_mod_handler('translate', true, $_tmp) : smarty_modifier_translate($_tmp)); ?>

</a>