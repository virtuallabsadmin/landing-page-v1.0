#lang scribble/manual

  @(require scribble/eval    ; <--- added

            "helper.rkt"     ; <--- added

            (for-label racket

                       "helper.rkt"))

  

  @title{My Library}

  

  @defmodule[my-lib/helper]{The @racketmodname[my-lib/helper]

  module---now with extra cows!}

  

  @defproc[(my-helper [lst list?])

           (listof (not/c (one-of/c 'cow)))]{

  

   Replaces each @racket['cow] in @racket[lst] with

   @racket['aardvark].

  

   @examples[

     (my-helper '())

     (my-helper '(cows such remarkable cows))

   ]}