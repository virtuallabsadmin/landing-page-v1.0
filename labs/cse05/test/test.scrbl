#lang scribble/manual
  @(require (for-label racket))
  @(require scribble/eval)
  @(require racket/include)
  
  @title{Recursive Programs}
  
  @itemize[
           @item{recursive programs on naturals}
           @item{recursive programs on lists} 
           ]

  @emph{Purpose : compute the factorial of a natural}
  
  Some example scheme codes for factorial:
  
  @section{Example-1}
  
  @interaction[
               (define !
                 (lambda (n)   
                   (cond
                     [(zero? n) 1]
                     [else (* n (! (- n 1)))])))
               (! 3)
               ]
  
  Observe the growing context around each call to !.

               (! 3)
               = (* 3 (! 2)
               = (* 3 (* 2 (! 1)))
               = (* 3 (* 2 (* 1 (! 0))))
  
  @section{Example-2}
  
  The accumulator version:
  
  @interaction[
               (define fac
                 (lambda (n)
                   (!/a n 1)))
               
               (define !/a
                 (lambda (n a)
                   (cond
                     [(zero? n) a]
                     [else (!/a (sub1 n) (* n a))])))
               (fac 3)                                      
                                                     ]
  Notice that the context does not grow.  This is because the recursive call to !/a is in a tail position.  An expression e is in a tail position wrt an enclosing expression e' if the evaluation of e is the last thing that happens in the evaluation of e'.



  @(include-section "bar.ss")
