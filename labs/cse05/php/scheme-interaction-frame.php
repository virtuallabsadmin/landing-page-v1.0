<html>

<link rel="stylesheet" type="text/css" href="../css/scheme-interaction.css"> 
<script language="JavaScript" src="../js/scheme-interaction.js"></script>

<head>

<title>PoPL Virtual Lab - Scheme</title>
<?php 
exec("rm ../tmp/*"); 
exec("rm ../usr/*"); 
?>

</head>

<body>

<div id="interface">
	<p>Enter your scheme code below and press "SHIFT+ENTER" to execute it :</p>
	<div id="input" class="input_class"> 
	<textarea rows='15' cols='55' name="code" id="command_id" class="command_class" value="" onkeypress="runScheme(event,this.value);" > </textarea>

	<div id="output_id" class="output_class">
	<!-- the output contect will go here -->
	</div>
	<input type='button' name='clear' value="Clear History" onclick="clearHistory();" />

	</div> 
</div>

</body>

</html>
