#lang scheme

(provide
 go
 go2a
 go2b)

;;; Solution 2
;;; ==========
;;; 1. Concrete Syntax
;;; ------------------
;;; <exp> ::= (& <list>) |
;;;           zzz |
;;;           (* <integer> <exp>) |
;;;           (+ <exp> <exp>) |


;;; 4. Semantic domains
;;; -------------------
;;; The expressible values for this language are lists.
;;; There are no identifiers in the language.  Hence, there
;;; denotable values domain isn't relevant here. Note that
;;; in the language zzz is a keyword, i.e., built into the
;;; concrete syntax.

(define expressible-value? list?)


;;; 5. Environments
;;; ---------------
;;; The question of environments does not arise because the
;;; language does not have identifiers.


;;; Method 1: Evaluator directly works on concrete syntax
;;; =====================================================

;;; 2. Abstract Syntax
;;; ------------------
;;;
;;; In this implementation, we operate directly on concrete
;;; syntax (i.e, we assume it is identical to the concrete
;;; syntax).  The evaluator directly operates on the
;;; concrete syntax.
;;;
;;;
;;; 3. Parser
;;; ---------
;;; Our AST and concrete syntax coincide, so the parsing is
;;; built into the evaluator (see below).


;;; 6. Evaluator
;;; ------------
;;; go : <exp> -> expressible-value?
;;; go : throws error if <exp> doesn't match concrete syntax specification.
(define go
  (lambda (d)
    (match d
      ['zzz '()]
      [(list '& (? list? ls)) ls]
      [(list '* (? integer? n) e) (repeat* n (go e))]
      [(list '+ e1 e2) (append (go e1) (go e2))]
      [_ (error 'go "invalid syntax: ~a" d)])))


;;; repeat* : [integer?  list?] -> list?
;;;
;;; use cases:
;;; (repeat* 0 '(a b))  =>  ()
;;; (repeat* 1 '(a b))  =>  (a b)
;;; (repeat* -1 '(a b)) =>  (b a)
;;; (repeat* 2 '(a b))  =>  (a b a b)
;;; (repeat* -2 '(a b)) =>  (b a b a)
(define repeat*
  (lambda (n ls)
    (let ([n (if (positive? n) n (- n))]
          [ls (if (positive? n) ls (reverse ls))])
      (letrec ([loop (lambda (k a)
                       (cond
                         [(zero? k) a]
                         [#t (loop (sub1 k) (append ls a))]))])
        (loop n '())))))


;;; Method 2:  Uses AST
;;; ====================

(require eopl/eopl)


;;; 2. Abstract Syntax
;;; -------------------
(define-datatype ast ast?
  [nil]
  [literal (ls list?)]
  [repeat (n integer?) (a ast?)]
  [concat (a1 ast?) (a2 ast?)])


;;; Evaluator
;;; ---------
;;; eval-ast: ast? -> expressible-value?
(define eval-ast
  (lambda (a)
    (cases ast a
      [nil () '()]
      [literal (ls) ls]
      [repeat (n a) (repeat* n (eval-ast a))]
      [concat (a1 a2) (append (eval-ast a1) (eval-ast a2))])))


;;; Method 2a: Parser uses match
;;; ----------------------------
;;; parse2a: <exp> -> ast? 
(define parse2a
  (lambda (d)
    (match d
      ['zzz (nil)]
      [(list '& (? list? ls)) (literal ls)]
      [(list '* (? integer? n) e) (repeat n (parse2a e))]
      [(list '+ e1 e2) (concat (parse2a e1) (parse2a e2))]
      [_ (error 'parse "invalid syntax: ~a" d)])))

;;; go2a: <exp> -> expressible-value?
(define go2a
  (lambda (e)
    (eval-ast (parse2a e))))


;;; Method 2b: Parser without match
;;; -------------------------------

;;; parse2b: <exp> -> ast? 
(define parse2b
  (lambda (e)
    (cond
     [(eq? e 'zzz) (nil)]
     [(list? e)
      (cond
       [(and
          (= (length e) 2)
          (eq? (first e) '&)
          (list? (second e)))
        (literal (second e))]
       [(and (= (length e) 3)
             (eq? (first e) '*)
             (number? (second e)))
        (repeat (second e) (parse2b (third e)))]
       [(and (= (length e) 3)
             (eq? (first e) '+)
        (concat (parse2b (second e)) (parse2b (third e))))]
       [#t (error 'parse2b "invalid syntax ~a" e)])]
     [#t (error 'parse2b "invalid syntax ~a" e)])))


;;; go2b: <exp> -> expressible-value?
(define go2b
  (lambda (e)
    (eval-ast (parse2b e))))

;;; End Solution 2
;;; ==============


;;; Unit Testing

(require rackunit)

(check-exn exn? (lambda () (go 5)))
(check-exn exn? (lambda () (go 'a)))
(check-exn exn? (lambda () (go #t)))
(check-exn exn? (lambda () (go "hello")))

(check-equal? (go '(& ())) '())
(check-equal? (go '(& (a b))) '(a b))
(check-equal? (go 'zzz) '())
(check-equal? (go '(* 3 (& (a b c)))) '(a b c a b c a b c))
(check-equal? (go '(* 1 (& (a b c)))) '(a b c))
(check-equal? (go '(* 0 (& (a b c)))) '())
(check-equal? (go '(* -1 (& (a b c)))) '(c b a))
(check-equal? (go '(* -3 (& (a b c)))) '(c b a c b a c b a))
(check-equal? (go '(+ (& (a 1 2)) (& (e f g)))) '(a 1 2 e f g))
(check-equal? (go '(+ zzz (& (a b c)))) '(a b c))


(check-exn exn? (lambda () (go2a 5)))
(check-exn exn? (lambda () (go2a 'a)))
(check-exn exn? (lambda () (go2a #t)))
(check-exn exn? (lambda () (go2a "hello")))

(check-equal? (go2a '(& ())) '())
(check-equal? (go2a '(& (a b))) '(a b))
(check-equal? (go2a 'zzz) '())
(check-equal? (go2a '(* 3 (& (a b c)))) '(a b c a b c a b c))
(check-equal? (go2a '(* 1 (& (a b c)))) '(a b c))
(check-equal? (go2a '(* 0 (& (a b c)))) '())
(check-equal? (go2a '(* -1 (& (a b c)))) '(c b a))
(check-equal? (go2a '(* -3 (& (a b c)))) '(c b a c b a c b a))
(check-equal? (go2a '(+ (& (a 1 2)) (& (e f g)))) '(a 1 2 e f g))
(check-equal? (go2a '(+ zzz (& (a b c)))) '(a b c))


(check-exn exn? (lambda () (go2b 5)))
(check-exn exn? (lambda () (go2b 'a)))
(check-exn exn? (lambda () (go2b #t)))
(check-exn exn? (lambda () (go2b "hello")))

(check-equal? (go2b '(& ())) '())
(check-equal? (go2b '(& (a b))) '(a b))
(check-equal? (go2b 'zzz) '())
(check-equal? (go2b '(* 3 (& (a b c)))) '(a b c a b c a b c))
(check-equal? (go2b '(* 1 (& (a b c)))) '(a b c))
(check-equal? (go2b '(* 0 (& (a b c)))) '())
(check-equal? (go2b '(* -1 (& (a b c)))) '(c b a))
(check-equal? (go2b '(* -3 (& (a b c)))) '(c b a c b a c b a))
(check-equal? (go2b '(+ (& (a 1 2)) (& (e f g)))) '(a 1 2 e f g))
(check-equal? (go2b '(+ zzz (& (a b c)))) '(a b c))

	
