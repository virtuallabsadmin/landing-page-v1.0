;;; POPL Lec 07 2010-08-24
;;; ======================




;;; Topics
;;; ======

;;; 1.  recursive programs on naturals
;;; 2.  recursive programs on lists


;;; Recursive functions on nat? and list?
;;; =====================================

;;; Purpose: compute the factorial of a natural

;;; Signature
;;; ---------
;;; !: nat? -> nat?

;; Implementation
;; --------------
(define !
    (lambda (n)   ;; n is a natural number
      (cond
        [(zero? n) 1]
        [else (* n (! (- n 1)))])))

;;; observe the growing context around each call to !.

;;; (! 2)
;;;   = (* 2 (! 1)
;;;   = (* 2 (* 1 (! 0)))



;;; ! using an accumulator.   

(define fac
  (lambda (n)
    (!/a n 1)))


;;; !/a : [nat? nat?] -> nat?

(define !/a
  (lambda (n a)
    (cond
      [(zero? n) a]
      [else (!/a (sub1 n) (* n a))])))


;;; trace of the accumulator version

(fac 2) 
      = (!/a 2 1)
      = (!/a 1 2*1)
      = (!/a 1 2)
      = (!/a 0 1*2)
      = (!/a 0 2)
      = 2

;;; Notice that the context does not grow.  This is because
;;; the recursive call to !/a is in a tail position.  An
;;; expression e is in a tail position wrt an enclosing
;;; expression e' if the evaluation of e is the last thing
;;; that happens in the evaluation of e'.   

;;; Invariant:
;;; (!/a n a)  = n!*a

;;; Exercise: formally prove this using mathematical induction


;;; Therefore (fac n) = (!/a n 1) = n!*1 = n!
;;; This completes the proof that (fac n) computes n factorial



;;; Recursive programs on lists
;;; ===========================


;;; Recall the inductive type list?

;;; -----------   NULL
;;; null: list?



;;; a: SchemeValue   ls: list?
;;; --------------------------   CONS
;;; (cons a ls) : list?



;;; Notice that we identify the inductive type lists with
;;; the predicate list?.
;;;

;;; constructors
;;; ------------
;;; null: list?
;;; cons: [any?, list?] -> list?

;;; We denote the type of SchemeValue by any? 

;;; Subtype predicates
;;; ------------------
;;; null? : list? -> boolean
;;; cons? : list? -> boolean


;;; projectors for cons
;;; -------------------
;;; first : cons? -> any?
;;; rest  : cons? -> list?


Recursive programs on lists
---------------------------


;;; General shape of recursive programs on lists.
;;; --------------------------------------------

;;; (define f
;;;   (lambda (ls)
;;;     (cond
;;;       [(null? ls) ...]
;;;       [else (...  (first ls)  (f (rest ls)))])))



;;; Examples:
;;; ---------

;;; list-length
;;; Purpose: computes the length of a list

;;; Signatue
;;; list-length : list? -> nat?


;; use cases
;; ---------

;; (list-length '()) = 0
;; (list-length '(a b c)) = 3

(define list-length
  (lambda (ls)
    (cond
      [(null? ls) 0]
      [else (+ 1
              (list-length (rest ls)))])))



;;; list-sum:
;;; ---------
;;; Purpose: sums all the numbers in a list of numbers
;;; list-num: (listof number?) -> number?

(define list-sum
  (lambda (ls)
    (cond
      [(null? ls) 0]
      [else (+
              (first ls)
              (list-sum (rest ls)))])))


;;; tracing list-sum


(list-sum '(2 3 5)) =
= (+ 2 (list-sum 3 5)) =
= (+ 2 (+ 3 (list-sum  5)))
= (+ 2 (+ 3 (+ 5 (list-sum  '()))))
= (+ 2 (+ 3 (+ 5 0)))
= (+ 2 (+ 3 5))
= (+ 2 8)
= 10


;;; list-prod: (listof number?) -> number?

(define list-prod
  (lambda (ls)
    (cond
      [(null? ls) 1]
      [else (*
              (first ls)
              (list-prod (rest ls)))])))
















