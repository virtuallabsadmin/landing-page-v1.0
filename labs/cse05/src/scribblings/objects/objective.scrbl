#lang scribble/doc

@(require scribble/manual
          scribble/bnf
          scribblings/scribble/utils
          (for-label scriblib/figure))

@title[#:tag "objective-10"]{Objective}

The objectives of this experiment are the following:

@itemlist[#:style 'ordered
	@item{Understanding the need for an @emph{object} in a programming language.}
	@item{The concept of @emph{encapsulation}.}
	@item{Implementation of an object oriented programming language.}
]
At the end of this experiment, the student is expected to have a clear understanding of the basic concepts of Object-oriented programming, 
   and also the design and implementation details of the same.
