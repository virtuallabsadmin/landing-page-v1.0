#lang scribble/doc

@(require scribble/manual
          scribble/bnf
          scribblings/scribble/utils
          (for-label scriblib/figure))

@title[#:tag "objective-2"]{Objective}


The objectives of this experiment are the following:

@itemlist[#:style 'ordered
	@item{Designing recursive procedures that operate on inductive types}
	@item{Define type  and equality predicates on inductive types}
]

At the end of this experiment, the student is expected to
know how to write recursive procedures that work with
inductive datatypes, and in particular the type and equality
predicates on inductive types.





