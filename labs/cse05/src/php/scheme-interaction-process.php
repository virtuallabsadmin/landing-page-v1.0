<?php

$schemeCode = stripslashes($_POST['code']);

$filename = "../tmp/code.scrbl";

if(file_exists($filename)) {
	$prevCode = file_get_contents($filename);
	$pattern = "/@interaction\[\n(.*)\n\]/ms";
	$num = preg_match($pattern, $prevCode, $matches);
	$schemeCode = $matches[1]."\n".$schemeCode;
}

$scribbleFile = fopen($filename, "w+");

$basic_code =
"#lang scribble/manual 
@(require (for-label racket))
@(require scribble/eval)
@interaction[\n".
	$schemeCode
."\n]";

fwrite($scribbleFile, $basic_code);
fclose($scribbleFile);

chdir("../usr/");
$ret_val = exec("scribble --dest ../tmp/ --html ../tmp/code.scrbl; python ../python/strip.py;");
chdir("../php/");

echo file_get_contents("../tmp/code.html");
?>
