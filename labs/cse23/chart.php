<html>
<head>
<script>
function show(){
	document.getElementById('list').style.visibility="visible";
}
function hide(){
	document.getElementById('list').style.visibility="hidden";
}

</script>
</head>
<body>
<?php
$target_path="uploads/";
$target_path=$target_path."temp.txt";
move_uploaded_file($_FILES['uploadedfile']['tmp_name'],$target_path);
chmod($target_path,0777);
$target_path="uploads/temp.txt";
$myfile=$target_path;
$fh=fopen($myfile,'r') or die("can't open file");

$theData="";
$wordCount=array();
$index=array();
$COUNT=0;


//negative count has been used so that they can finally be sorted in decreasing order of their count

while(!feof($fh)){
	$theData = fgets($fh);
	$words=preg_split("/[:\s,]+/",$theData);
	for($i=0;$i<count($words);$i=$i+1){
		if($words[$i]!=""){
			$COUNT+=1;
			if(array_key_exists($words[$i],$wordCount)){
				$wordCount[$words[$i]]-=1;
			}
			else{
				$wordCount[$words[$i]]=-1;
			}
		}
	}
}

fclose($fh);
asort($wordCount);

$Max=0;

foreach($wordCount as $key => $val){
//	$val=$val*-1;
	if(array_key_exists($val,$index)){
		$index[$val]=$index[$val]." ".$key;
	}
	else{
		$index[$val]=$key;
	}
	if($Max<$val){
		$Max=$val;
	}
}

$map=array();

foreach($index as $key => $val){
	$temp=preg_split("/[\s]+/",$val);
	$map[$key]=count($temp);
}
$threshold=$_POST['threshold'];
//print_r(array(2,1));
include_once 'open-flash-chart-1.9.7/php-ofc-library/open_flash_chart_object.php';
open_flash_chart_object( 400, 200, 'http://'. $_SERVER['SERVER_NAME'] .'/CL/chart-data.php?th=-1', false );
open_flash_chart_object( 400, 200, 'http://'. $_SERVER['SERVER_NAME'] .'/CL/chart-data.php?th='.$threshold, false );
?>

<br>
<br>
<div align="center"><input type="button" onClick="show()" value="Word-Frequency List" />
<div id="list" style="visibility:hidden;">
<?php
echo "<h2>word  =  frequency</h2>";
$myFile="/CL/uploads/tokens.txt";
//chmod($myFile,0777);
$fw=fopen($myFile,'w') or die("can't open file"); 
foreach($wordCount as $key=>$val){
	if(-1*$val >= $threshold){
		$str=$key."\n";
		fwrite($fw,$str);
		echo $key."  =  ".-1*$val."<br>";
	}
}
fclose($fw);
?>
<input type="button" onClick="hide()" value="hide List" />
</div>
</div>
</body>
</html>
