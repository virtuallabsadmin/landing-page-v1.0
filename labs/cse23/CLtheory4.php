<html>
<body>
<b>Coverage</b> of a unique word in a text signifies its percentage frequency given the total number of words present in the text. Instead of using the term 'word' it might be better to use the term 'word token' (or a token). So the coverage of a token x
<br/><br />
<div style="border:1px solid green; padding-left:4px;"><br>
<b>Coverage (x) = Total no. of instance of x / Total no. of all the token in the text.</b><br><br>
</div>
<br/>
According to our definition different forms of a same word are unique token. 'go', 'went', 'goes', etc. are all treated as different token. This is different from the notion of 'word form' where these differences are abstracted away and these will have the same form 'go'.
<br/><br/>
The notion of coverage of words in a text with respect to dictionary making gives us a criterion for work selection. Sometimes, instead of selecting all the words in a text it might be advantageous (in terms of man hours) to select a set of words that can maximize our coverage.
<br/><br/>
</body>
</html>
