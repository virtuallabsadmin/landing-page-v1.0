<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />  
<script src="jquery.js"  type="text/javascript" ></script>
	<script type="text/javascript" src="./jquery.fancybox-1.3.1/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
	<script type="text/javascript" src="./jquery.fancybox-1.3.1/fancybox/jquery.fancybox-1.3.1.js"></script>
	<link rel="stylesheet" type="text/css" href="./jquery.fancybox-1.3.1/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
<script type="text/javascript">
	$(document).ready(function() {
		$("a#example1").fancybox({
			'titleShow'         : false
			});

		});
</script>

<script type="text/javascript">
var state="hidden";
function hide(layerId){
	var elem,vis;
	if(document.getElementById){
		elem=document.getElementByIdO(layerId);
	}
	else if(document.all){
		elem=document.all[layerId];
	}
	else if(document.layers){
		elem=document.layers[layerId];
	}
	vis=elem.styledisplay="none";

}	
function show(layerId){
//	alert(layerId);
	$('.divClass').fadeOut(100);
	$('#'+layerId).css("display","block");
}
</script>
</head>
<body>
<header id="site_head">
<title>CL</title>
		<div class="header_cont"> <!-- 960px header-->
			<!-- LOGO of the company-->
			<div id="logo">
		<!--	<h1> <a href="index.php">Language Technology Research Centre. </a> </h1>-->
			<h1> <a href="index.php">Computational Linguistics Virtual Labs. </a> </h1>
            
			</div>
	  </div>
			<!-- Menu -->
	</header>

<script type="text/javascript">
$('document').ready(function (){

$('sidelink').click(function (){
$('.divs').css('display','none');
ids=this.id;
alert(ids);
$('#div'+ids).fadeIn(500);
	
});


});
</script>

<div id="wrap">
<div id="header">
<?php
$E=$_GET["Exp"];
$sel=$_GET["sel"];
$temp="Experiment ".$E;
echo $temp;
?>
<br><br><hr>
<?php
echo "<a href=\"CLExp.php?Exp=".$E."&p=i\" >Introduction</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"CLExp.php?Exp=".$E."&p=t\" >Theory</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"CLExp.php?Exp=".$E."&p=o\" >Objective</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"CLExp.php?Exp=".$E."&p=p\" >Procedure</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"CLExp.php?Exp=".$E."&p=e\" >Experiment</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"CLExp.php?Exp=".$E."&p=q\" >Quiz</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
echo "<a href=\"CLExp.php?Exp=".$E."&p=r\" >Reference</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
?>
<hr>
</div>
<div id="content">
<div class="menu">
<!--the right most-->


</div>


<?php
$arr["t"]="none";
$arr["o"]="none";
$arr["e"]="none";
$arr["p"]="none";
$arr["q"]="none";
$arr["i"]="none";
$arr["r"]="none";

$title[1]="Understanding basic Parts of speech";
$title[2]="Marking basic Parts of speech";
$title[3]="Marking fine-grained Parts of speech";
$title[4]="Understanding Word Coverage";
$title[5]="Word sense disambiguation";
$title[6]="Dictionary generation";
$title[7]="Marking Parts of speech";

if(isset($_GET["p"])){
//echo "<SCRIPT LANGUAGE='javascript'>show('experiment');</SCRIPT>";	
$var=$_GET["p"];
$arr[$var]="block";
}
else
{
$arr["i"]="block";
}
?>
<!--the middle one-->

<div id="theory" class='divClass' style="display:<?php echo $arr["t"];?>;">
<?php
	echo '<h2 align="center">'.$title[$E].'</h2><br>';
	echo "<h3 align='center'> Theory</h3><br>";
	$str="CLtheory".$E.".php";
	include($str);
?>
</div>

<div id="objective" class='divClass'  style="display:<?php echo $arr["o"];?>;">
<?php
	echo '<h2 align="center">'.$title[$E].'</h2><br>';
	echo "<h3 align='center'> Objective</h3><br>";
	$str="CLobjective".$E.".php";
	include($str);
?>
</div>

<div id="procedure" class='divClass'   style="display:<?php echo $arr["p"];?>;">
<?php
	echo '<h2 align="center">'.$title[$E].'</h2><br>';
	echo "<h3 align='center'> Procedure</h3><br>";
	$str="CLprocedure".$E.".php";
	include($str);
?>
</div>

<div id="experiment" class='divClass'   style="display:<?php echo $arr["e"];?>;">
<?php
	echo '<h2 align="center">'.$title[$E].'</h2><br>';
	echo "<h3 align='center'> Experiment</h3><br>";
	$str="CLexperiment".$E.".php";
	include($str);
?>
</div>

<div id="quiz" class='divClass'   style="display:<?php echo $arr["q"];?>;">
<?php
	echo '<h2 align="center">'.$title[$E].'</h2><br>';
	echo "<h3 align='center'> Quiz</h3><br>";
	$str="CLquiz".$E.".php";
	include($str);
?>
</div>

<div id="introduction" class='divClass' style="display:<?php echo $arr["i"];?>;">
<?php
	echo '<h2 align="center">'.$title[$E].'</h2><br>';
	echo "<h3 align='center'> Introduction</h3><br>";
	$str="CLintroduction".$E.".php";
	include($str);
?>
</div>

<div id="reference" class='divClass' style="display:<?php echo $arr["r"];?>;">
<?php
	echo '<h2 align="center">'.$title[$E].'</h2><br>';
	echo "<h3 align='center'> Reference</h3><br>";
	$str="CLreference".$E.".php";
	include($str);
?>
</div>

<div class='cleared'></div>
</div>

<div id="bottom" style="padding-top:20px; padding-left:5px;">
<?
//include('footer.php');
?>
</div>
</div>
</body>
</html>
