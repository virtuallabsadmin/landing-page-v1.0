<html>

<head>
<style type="text/css">
tr.BOLD{
	font-weight:bold;
}

</style>
</head>

<body>
<hr><br>
To understand and test the knowledge of more fine-grained part of speech of words as they appear in a sentence.<br><br>
View <a id="example1" href="./penn.html">Tagset</a>.<br><br>
<div align="left" style="font-size:14px;"><b><u>Example 1;</u></b>The child liked to play.</div><br>
<table width="100%">
<tr><td>The</td><td>child</td><td>liked</td><td>to</td><td>play</td><td>.</td>
</tr>
<tr class="BOLD"><td>DT</td><td>NN</td><td>VBD</td><td>TO</td><td>VB</td><td>SYM</td>
</tr>
</table><br>
<div align="left" style="font-size:14px;"><b><u>Example 2;</u></b> The play that Bill Attended was very good.</div><br>
<table width="100%">
<tr><td>The</td><td>play</td><td>that</td><td>Bill</td><td>Attended</td><td>was</td><td>very</td><td>good</td><td>.</td></tr>
<tr class="BOLD">
<td>DT</td><td>NN</td><td>CC</td><td>NNP</td><td>VBN</td><td>VBD</td><td></td><td>JJ</td><td>SYM</td>
</tr>
</table>
<br>
<div align="left" style="font-size:14px;"><b><u>Example 3;</u></b> The scene of the play is a place where children play.</div><br>
<table width="100%">
<tr>
<td>The</td><td>scene</td><td>of</td><td>the</td><td>play</td><td>is</td><td>a</td><td>place</td><td>where</td><td>children</td><td>play</td><td>.</td>
</tr>
<tr class="BOLD">
<td>DT</td><td>NN</td><td>IN</td><td>DT</td><td>NN</td><td>VBZ</td><td>DT</td><td>NN</td><td>WRB</td><td>NN</td><td>VBZ</td><td>SYM</td>
</tr>
</table>
<br>
<div align="left" style="font-size:14px;"><b><u>Example 4;</u></b>A child in the play liked to place the green bill on the red flower, and used to wonder which of them was beautiful.</div><br>
<table width="100%">
<tr>
<td>A</td><td>child</td><td>in</td><td>the</td><td>play</td><td>liked</td><td>to</td><td>place</td><td>the</td><td>green</td><td>bill</td><td>on</td><td>the</td><td>red</td>
</tr>
<tr class="BOLD">
<td>DET</td><td>NN</td><td>IN</td><td>DT</td><td>NN</td><td>VBZ</td><td>TO</td><td>VB</td><td>DT</td><td>JJ</td><td>NN</td><td>IN</td><td>DET</td><td>JJ</td>
</tr>
</table>
<table width="100%">
<tr>
<td>flower</td><td>,</td><td>and</td><td>used</td><td>to</td><td>wonder</td><td>which</td><td>of</td><td>them</td><td>was</td><td>beautiful</td><td>.</td>
</tr>
<tr class="BOLD">
<td>NN</td><td>SYM</td><td>CC</td><td>VBD</td><td>TO</td><td>VB</td><td>WDT</td><td>IN</td><td>PRP</td><td>VBD</td><td>JJ</td><td>SYM</td>
</tr>
</table>
<br>
<hr>
</body>
</html>
