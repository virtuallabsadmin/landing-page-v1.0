<html>
<head>
<script type="text/javascript">
var count=0;
var userClass=new Array();
var wClass=new Array();
var wordList=new Array();

function contains(word){
	for(i=0;i<wordList.length;i++){
		if(wordList[i]==word){
			return i;
		}
	}
	return -1;
}

function result(){
	sentence="<table width='100%' bgcolor=#FFD4A8 border='0'><tr><th>Word</th><th>Your Answer</th><th>Correct Answer</th></tr>";
	for(i=0;i<count;i++){
		sentence=sentence+"<tr><td align='center'>" +wordList[i]+"</td><td align='center'>" +userClass[i] +"</td><td align='center'>" +wClass[i]+"</td></tr>";
	}
	sentence=sentence+"</table>";
//	alert(sentence);
	document.getElementById("result").innerHTML=sentence;
}


function checker(wc,word){
	index=contains(word);
	xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4){
			temp=xmlhttp.responseText;
		//	alert(temp);
			if(index==-1){
				wClass[count]=xmlhttp.responseText;
				wordList[count]=word;
				userClass[count]=wc;
				count=count+1;
			}
			else{
				userClass[index]=wc;
			}
		}
	}
	xmlhttp.open("GET","getClass.php?w="+word+"&Q=2",true);
	xmlhttp.send();
}
var chunks;
function option(word){
	for(i=0;i<chunks.length;i++){
		document.getElementById(chunks[i]).style.backgroundColor="#003300";
		if(chunks[i]==word){
			document.getElementById(word).style.backgroundColor="red";
		}
	}
	options="<input type='button' onclick='checker(\"noun\",\""+word+"\");' class='myoptbutton' value='Noun'>&nbsp";
	options=options+ "<input type='button' onclick='checker(\"pronoun\",\""+word+"\");' class='myoptbutton' value='Pronoun'>&nbsp";
	options=options+ "<input type='button' onclick='checker(\"verb\",\""+word+"\");' class='myoptbutton' value='Verb'>&nbsp";
	options=options+ "<input type='button' onclick='checker(\"adjective\",\""+word+"\");' class='myoptbutton' value='Adjective'>&nbsp";
	options=options+ "<input type='button' onclick='checker(\"determiner\",\""+word+"\");' class='myoptbutton' value='Determiner'>&nbsp";
	options=options+ "<br><br><input type='button' onclick='result();' class='myoptbutton' value='Check!!'>&nbsp";
	document.getElementById("options").innerHTML=options;
}

function setValue(data){
	document.getElementById("options").innerHTML="";
	document.getElementById("result").innerHTML="";
	document.getElementById("sent").innerHTML="";
	if(data=="null"){
		document.getElementById("sent").innerHTML="Select a Sentence!!";
		return;
	}
	count=0;
	chunks=data.split(" ");
	sentence="Click on the word to mark POS<br><br>";
	for(i=0;i<chunks.length;i++){
		sentence=sentence+"&nbsp<input type='button' id='"+chunks[i]+"'  onclick='option(\""+chunks[i]+"\");'  class='mybutton' value='"+chunks[i] + "'>";
	}
	sentence=sentence+".";
	document.getElementById("sent").innerHTML=sentence;
}

</script>


</head>
<style type="text/css">

input.mybutton{
	font-size:14px;
	font-family:Trebuchet MS,sans-serif;
	font-weight:bold;
	color:#FFFFFF;
	height:19px;
       background-color:#003300;
       border-style:none;
}
input.myoptbutton{
	font-size:16px;
	font-family:Arial,sans-serif;
	height:26px;
       background-color:#779999;
       background-image:url(back03.gif);
       border-style:solid;
       border-color:#DDDDDD;
       border-width:1px;
}


</style>

<body>
<div align="center">
<select name="sentence">
<option selected="selected" onclick="setValue('null')">Select a sentence</option>
<option onclick="setValue('ram is a good boy')">Ram is a good boy</option>
<option onclick="setValue('he runs fast')">He runs fast</option>
</select>
<br><br>
<div id="sent"></div>
<br>
<div id="options"></div><br>
<div id="result"></div>
</div>

</body>
</html>
