<html>
<head>
<script type="text/javascript">
var count=0;
var userClass=new Array();
var wClass=new Array();
var wordList=new Array();
var chunks;
var Glword;

function contains(word){
	for(i=0;i<wordList.length;i++){
		if(wordList[i]==word){
			return i;
		}
	}
	return -1;
}

function result(){
	sentence="<table width='100%' bgcolor=#FFD4A8 border='0'><tr><th>Word</th><th>Your Answer</th><th>Correct Answer</th></tr>";
	for(i=0;i<count;i++){
		sentence=sentence+"<tr><td align='center'>" +wordList[i]+"</td><td align='center'>" +userClass[i] +"</td><td align='center'>" +wClass[i]+"</td></tr>";
	}
	sentence=sentence+"</table>";
//	alert(sentence);
	document.getElementById("result").innerHTML=sentence;
}

function layout(word){
	for(i=0;i<chunks.length;i++){
		document.getElementById(chunks[i]).style.backgroundColor="#003300";
		if(chunks[i]==word){
			document.getElementById(word).style.backgroundColor="red";
		}
	}
	Glword=word;
//	alert("hello");
	document.getElementById("layout").style.display='block';
}
function newcheck(wc){
//	alert(cl + " " + Glword);
	index=contains(Glword);
//	alert(index);
	xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4){
			temp=xmlhttp.responseText;
			if(index==-1){
				wClass[count]=xmlhttp.responseText;
				wordList[count]=Glword;
				userClass[count]=wc;
				count=count+1;
			}
			else{
				userClass[index]=wc;
			}
		}
	}
	xmlhttp.open("GET","getClass.php?w="+Glword+"&Q=3" ,true);
	xmlhttp.send();
}

function setValue(data){
	count=0;
	document.getElementById("sent").innerHTML="";
	if(data=="null"){
		document.getElementById("sent").innerHTML="Select a Sentence!!";
		document.getElementById("layout").innerHTML="";
		return;
	}
	chunks=data.split(" ");
	sentence="Click on a word to mark POS<br><br>";
	for(i=0;i<chunks.length;i++){
		sentence=sentence+"&nbsp<input type='button' id='"+chunks[i]+"'  onclick='layout(\""+chunks[i]+"\");'  class='mybutton' value='"+chunks[i] + "'>";
	}
	sentence=sentence+".";
	document.getElementById("sent").innerHTML=sentence;
}

</script>


</head>
<style type="text/css">

input.mybutton{
	font-size:14px;
	font-family:Trebuchet MS,sans-serif;
	font-weight:bold;
	color:#FFFFFF;
	height:19px;
       background-color:#003300;
       border-style:none;
}
input.myoptbutton{
	font-size:16px;
	font-family:Arial,sans-serif;
	height:26px;
       background-color:#779999;
       background-image:url(back03.gif);
       border-style:solid;
       border-color:#DDDDDD;
       border-width:1px;
}
</style>
<body>
<div align="center">
To see the Tagset <a id="example1" href="./penn.html">Click here</a>.
<br>
<br>
<select name="sentence">
<option selected="selected" onclick="setValue('null')">Select a sentence</option>
<option onclick="setValue('ram is a good boy')">Ram is a good boy</option>
<option onclick="setValue('he runs fast')">He runs fast</option>
</select>
<br><br>
<div id="sent"></div>
<br>
<div id="layout" style="display:none;">
<?php
include("layout.php");
?>
</div><br>
<div id="result"></div>
</div>
</body>
</html>
