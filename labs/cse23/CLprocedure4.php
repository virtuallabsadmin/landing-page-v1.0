<html>
<body>
<b><u>STEP1: </u></b>Choose a file to be uploaded (A default file is already present in case you don't have one).<br>
<b><u>STEP2: </u></b> Give the threshold value (Default is 0).<br>
<b><u>STEP3: </u></b> upload the file.<br>
<b><u>OUTPUT1: </u></b>Frequency-Count and Coverage graphs will appear.<br><br>
<b><u>STEP4: </u></b> You can further view the tokens with their frequency by pressing the button <button>Word-frequency List</button>.<br>
<b><u>STEP5: </u></b> <button>view uploaded file</button>can be used to view the uploaded file (default file in case no file is uploaded).<br>
<br><hr>
</body>
</html>
