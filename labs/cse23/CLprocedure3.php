<html>
<body>
<b><u>STEP1: </u></b> Select the sentence from drop down menu.<br>
<b><u>OUTPUT1: </u></b> The selected sentence will come with each word as a pressable button.<br><br>
<b><u>STEP2: </u></b> Press each of the words (pressable buttons).<br>
<b><u>OUTPUT2: </u></b>The pressed word will turn <font color="red">RED</font> and Tagset will appear :<br>
<b><u>STEP3: </u></b> Select you answer from these choices.<br>
<b><u>STEP4: </u></b> After you have finished with all the words you can click on check to check your answers.<br><br>
<b><u>NOTE: </u></b> You can also check in between of the sentence but only those words which you have selected will be shown as the answer.<br>
<br><hr>

</body>
</html>
