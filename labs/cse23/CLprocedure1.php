<html>
<body>
<b><u>STEP1: </u></b> Select the sentence from drop down menu.<br>
<b><u>OUTPUT1: </u></b> The selected sentence will come with each word as a pressable button.<br>
<b><u>STEP2: </u></b> Press each of the words (pressable buttons).<br>
<b><u>OUTPUT2: </u></b>The pressed word will be classified as one of the following categories:<br>
<b>Noun, Pronoun, Verb, Determiner, Adjective.</b>
<br><br><hr>
</body>
</html>
