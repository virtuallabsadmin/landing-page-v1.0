#include <stdlib.h>
#define cnSize 3
int A[cnSize][cnSize];
int B[cnSize][cnSize];
int C[cnSize][cnSize];
int x[cnSize];
int y[cnSize];

int i, j, k;

// fill matrix and vector with random values
void randinit(){
  srand ( time(NULL) );
  for (i = 0; i < cnSize; ++i) {
    for (j = 0; j < cnSize; ++j) {
      A[i][j] = rand()%1000;
      B[i][j] = rand()%1000;
    }
    x[i] = rand()%1000;
  }
}

// matrix-vector multiplication
void matvec(){
  for (i = 0; i < cnSize; ++i){
    y[i] = 0;
    for (k = 0; k < cnSize; ++k){
        y[i] += A[i][k] * x[k];
    }
  }
}

// matrix-matrix multiplication
void matmult(){
  for (i = 0; i < cnSize; ++i){
    for (j = 0; j < cnSize; ++j){
      C[i][j] = 0;
      for (k = 0; k < cnSize; ++k){
        C[i][j] += A[i][k] * B[k][j];
      }
    }
  }
}

main(){
  randinit();
  matmult();
}
 
