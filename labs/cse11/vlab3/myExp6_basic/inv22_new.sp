*inverter
.include '180nm_bulk.txt'
M1 out vb 0 0 NMOS l= w=
M2 vb vb 0 0 NMOS l= w=
Ib Vdd vb m
Vdd Vdd 0 1.8
Vdc out 0 dc 
.dc Vdc 0 1.8 10m
.save dc I(Vdc) V(out)
.end
