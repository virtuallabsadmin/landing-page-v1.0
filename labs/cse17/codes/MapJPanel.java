/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Abhi
 */
import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;

public class MapJPanel extends javax.swing.JPanel
{

   public ArrayList<Rect> obstacles;
   Pose robotPoseCurrOld;
   Pose robotPoseCurrCorrected;
   Pose robotPoseReal;
   int flag;
   private int WIDTH;
   private int HEIGHT;

   public MapJPanel(int WIDTH, int HEIGHT)
   {

      super();
      setWIDTH(WIDTH);
      setHEIGHT(HEIGHT);
      flag = 0;
      robotPoseCurrCorrected=new Pose();
      robotPoseCurrOld=new Pose();
      robotPoseReal=new Pose();
   }

   public void setRobotPoseCurrOld(Pose P)
   {
      robotPoseCurrOld = new Pose(P);
      //     repaint();
   }

   public void setRobotPoseCurrCorrected(Pose P)
   {
      robotPoseCurrCorrected = new Pose(P);
      //   repaint();
   }

   public void setRobotPoseReal(Pose P)
   {
      robotPoseReal = new Pose(P);
      // repaint();
   }

   void reset()
   {
      /*    scanCurrOld = new ArrayList<POint>();
      scanCurrCorrected = new ArrayList<POint>();
      scanReal = new ArrayList<POint>();
       */
      robotPoseCurrCorrected = new Pose();
      repaint();
   }

   protected void paintComponent(Graphics g)
   {

      super.paintComponent(g);



      g.setColor(Color.black);


//      *****Drawing Obstacles

      for(int i = 1; i < obstacles.size(); i++)
      {
         Rect rct = obstacles.get(i);
         POint p = rct.getCornerD();
         if(i != 0)
         {
            g.fillRect((int) p.getX(), HEIGHT - (int) p.getY(), (int) rct.getWidth(), (int) rct.getHeight());
         } else
         {
            g.fillRect((int) p.getX(), HEIGHT - (int) p.getY(), (int) rct.getWidth(), (int) rct.getHeight());
         }

      }

      POint p1, p2;

//      *****Drawing robotPoseCurrOld
 //     robotPoseReal.display();
      if(!(robotPoseReal.getAngle() == 0 && robotPoseReal.getP().getX() == 0 && robotPoseReal.getP().getY() == 0))
      {
       //  System.out.println("asdf");
         g.setColor(Color.BLACK);
         g.drawOval((int) robotPoseReal.getP().getX() - 9, HEIGHT - (int) robotPoseReal.getP().getY() - 9, 18, 18);
         p1 = new POint(robotPoseReal.getP());
         p2 = new POint(p1.getX() + 9 * Math.cos(Math.toRadians(robotPoseReal.getAngle())), p1.getY() + 9 * Math.sin(Math.toRadians(robotPoseReal.getAngle())));
       //  g.drawLine((int) p1.getX() - 1, HEIGHT - (int) p1.getY() - 1, (int) p2.getX()-1, HEIGHT - (int) p2.getY()-1);
          g.drawLine((int) p1.getX(), HEIGHT - (int) p1.getY(), (int) p2.getX(), HEIGHT - (int) p2.getY());
        //   g.drawLine((int) p1.getX()+1, HEIGHT - (int) p1.getY()+1, (int) p2.getX()+1, HEIGHT - (int) p2.getY()+1);
    //      g.fillOval((int) p2.getX() , HEIGHT - (int) p2.getY() , 4, 4);
      }

//      *****Drawing robotPoseCurrCorrected
      if(!(robotPoseCurrOld.getAngle() == 0 && robotPoseCurrOld.getP().getX() == 0 && robotPoseCurrOld.getP().getY() == 0))
      {
         g.setColor(Color.RED);
         g.drawOval((int) robotPoseCurrOld.getP().getX() - 9, HEIGHT - (int) robotPoseCurrOld.getP().getY() - 9, 18, 18);
         p1 = new POint(robotPoseCurrOld.getP());
         p2 = new POint(p1.getX() + 9 * Math.cos(Math.toRadians(robotPoseCurrOld.getAngle())), p1.getY() + 9 * Math.sin(Math.toRadians(robotPoseCurrOld.getAngle())));
         g.drawLine((int) p1.getX(), HEIGHT - (int) p1.getY(), (int) p2.getX(), HEIGHT - (int) p2.getY());
      }

      if(!(robotPoseCurrCorrected.getAngle() == 0 && robotPoseCurrCorrected.getP().getX() == 0 && robotPoseCurrCorrected.getP().getY() == 0))
      {
         g.setColor(Color.GREEN);
         g.drawOval((int) robotPoseCurrCorrected.getP().getX() - 9, HEIGHT - (int) robotPoseCurrCorrected.getP().getY() - 9, 18, 18);
         p1 = new POint(robotPoseCurrCorrected.getP());
         p2 = new POint(p1.getX() + 9 * Math.cos(Math.toRadians(robotPoseCurrCorrected.getAngle())), p1.getY() + 9 * Math.sin(Math.toRadians(robotPoseCurrCorrected.getAngle())));
         g.drawLine((int) p1.getX(), HEIGHT - (int) p1.getY(), (int) p2.getX(), HEIGHT - (int) p2.getY());
         flag = 1;
      }
   }

   /**
    * @return the WIDTH
    */
   public int getWIDTH()
   {
      return WIDTH;
   }

   /**
    * @param WIDTH the WIDTH to set
    */
   public void setWIDTH(int WIDTH)
   {
      this.WIDTH = WIDTH;
   }

   /**
    * @return the HEIGHT
    */
   public int getHEIGHT()
   {
      return HEIGHT;
   }

   /**
    * @param HEIGHT the HEIGHT to set
    */
   public void setHEIGHT(int HEIGHT)
   {
      this.HEIGHT = HEIGHT;
   }
}
