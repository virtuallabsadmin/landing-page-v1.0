/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 *
 * @author Abhi
 */
public class Pose {
    private POint p;
    private double angle;
    /**
     *
     */
    public Pose()
    {
            p=new POint();
            angle=0;
    }
    public Pose(Pose n){
       setP(new POint(n.getP()));
       setAngle(n.angle);
       }



    public Pose(double x,double y,double ang)
    {
        p=new POint(x,y);
            angle=ang;
    }


    public Pose sum(Pose temp){

        return new Pose(getP().getX()+temp.getP().getX(),getP().getY()+temp.getP().getY(),getAngle()+temp.getAngle());
    }
    /**
     * @return the p
     */
    public POint getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(POint p) {
        this.p = new POint(p);
    }

    /**
     * @return the angle
     */
    public double getAngle() {
        return angle;
    }

    /**
     * @param angle the angle to set
     */
    public void setAngle(double angle) {
        this.angle = angle;
    }
   /* Pose operator+(pose & other)
    {
            return pose(p.x+other.p.x,p.y+other.p.y,angle+other.angle);
    }*/
    public void display(){
        getP().display();
         System.out.print(",");
          System.out.format("%.2f\n",getAngle());

    }
     /**
     *
     * @param temp
     * @return
     */
//     public bool checkZero()
//   {
//     }
}