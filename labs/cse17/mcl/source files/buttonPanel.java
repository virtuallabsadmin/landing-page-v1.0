import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSlider;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.GridLayout;
import java.awt.BorderLayout;
import javax.swing.JComboBox;

public class buttonPanel extends JPanel
{
	public JButton start;
	public JButton sense;
	public JButton move;
	public JSlider range;
	public JLabel rangeLabel;
	public JLabel mapLabel;
	public JComboBox Maps;
	public String[] maps={"Map 1", "Map 2", "Map 3"};

	public buttonPanel()
	{
		start=new JButton("Start/Restart");
		sense=new JButton("Sense");
		move=new JButton("Move");
		rangeLabel=new JLabel("              Range Label: ");
		mapLabel=new JLabel("         Map: ");
		
		Maps =new JComboBox(maps);
		Maps.setMaximumRowCount(3);
		range=new JSlider(JSlider.HORIZONTAL,0,100,25);
		range.setMajorTickSpacing(50);	
		range.setMinorTickSpacing(10);
		range.setPaintTicks(true);
		range.setPaintLabels(true);
		add(start);
		add(sense);
		add(move);
		add(rangeLabel);
		add(range);
		add(mapLabel);
		add(Maps);
	
	}
	
	public static void main(String[] args)
	{
		buttonPanel bp=new buttonPanel();
		JFrame frame= new JFrame("Soup");
		frame.setLayout(new BorderLayout());
		frame.add(bp, BorderLayout.NORTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(250,500);
		frame.setVisible(true);
	}

}
