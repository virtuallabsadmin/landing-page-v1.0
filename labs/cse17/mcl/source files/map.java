import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import java.lang.Math;
import java.awt.Color;
import java.awt.event.MouseAdapter;

public class map extends JPanel
{
	public int[] xPoints1={20,20,140,140,380,380,300,300,180,180,480,480};	
	public int[] yPoints1={20,480,480,140,140,360,360,180,180,480,480,20};	
	public int numOfSides1=12;
	public Pose robot1=new Pose(80,460,0);

	public int[] xPoints2={60,60,20,20,120,120,160,160,200,200,320,320,240,240,320,320,20,20,480,360,360,420,420,460,460,410,410,480,480,370,370,100,100};
	public int[] yPoints2={40,120,120,160,160,200,200,160,160,280,280,340,340,400,400,440,440,480,480,360,180,180,230,230,140,140,100,100,40,40,120,120,40};
	public int numOfSides2=33;
	public Pose robot2=new Pose(80,460,0);

	public int[] xPoints3={20,20,70,70,120,120,200,200,120,120,70,70,20,20,480,480,380,380,430,430,360,360,280,280,360,360,430,430,480,480};
	public int[] yPoints3={20,240,240,100,100,220,220,280,280,420,420,280,280,480,480,300,300,360,360,420,420,280,280,220,220,100,100,240,240,20};
	public int numOfSides3=30;
	public Pose robot3=new Pose(80,460,0);

	public int[] xPoints;
	public int[] yPoints;

	public int numOfSides;

	public double maxRange=50;
	public double sensorSigma=0.5;
	public double measurementVariance=0.5;
	public double deltaR=5;

	//Velocoty Model Parameters
	public double sigmaTheta=0.003;	
	public double sigmaR=0.0005;

	public Polygon borders;
	public Random random=new Random();
	public double[] SensorDirections={Math.toRadians(0),Math.toRadians(90),Math.toRadians(180),Math.toRadians(270)};
	public Pose robot;
	public double[] Measurements;
	public int numOfParticles;
	public double[] Weight;
	public double[] AccWeight;
	public Pose[] Particle;
	public Pose[] tempParticle;
	public Pose[] swap;
	public boolean isLocal;

	public map()
	{
		numOfParticles=2000;
		Particle=new Pose[numOfParticles];
		AccWeight=new double[numOfParticles];
		tempParticle=new Pose[numOfParticles];
		swap=new Pose[numOfParticles];
		AccWeight=new double[numOfParticles];
		Weight=new double[numOfParticles];
		for(int i=0;i<numOfParticles;i++){Weight[i]=1.0/numOfParticles;}
		InitMapLocal(0);
		isLocal=true;
	}
	
	public void InitMapLocal(int index)
	{
		if(index==0){xPoints=xPoints1;yPoints=yPoints1;numOfSides=numOfSides1;robot=robot1;}
		if(index==1){xPoints=xPoints2;yPoints=yPoints2;numOfSides=numOfSides2;robot=robot2;}
		if(index==2){xPoints=xPoints3;yPoints=yPoints3;numOfSides=numOfSides3;robot=robot3;}
		borders=new Polygon(xPoints,yPoints,numOfSides);
		for(int i=0;i<numOfParticles;i++){Particle[i]=robot1;}
		repaint();
	}

	public void InitMapGlobal(int index)
	{
		if(index==0){xPoints=xPoints1;yPoints=yPoints1;numOfSides=numOfSides1;robot=robot1;}
		if(index==1){xPoints=xPoints2;yPoints=yPoints2;numOfSides=numOfSides2;robot=robot2;}
		if(index==2){xPoints=xPoints3;yPoints=yPoints3;numOfSides=numOfSides3;robot=robot3;}
		borders=new Polygon(xPoints,yPoints,numOfSides);
		for(int i=0;i<numOfParticles;i++)	{Particle[i]=CreateRandomPose();}
		repaint();
	}

	public Pose CreateRandomPose()
	{
		double newHeading=random.nextDouble()*360;
		double newX,newY;
		do{
			newX=random.nextDouble()*500;
			newY=random.nextDouble()*500;
		}while(borders.contains(newX,newY)==false);
		return new Pose(newX,newY,newHeading);
	}

	public void Sense()
	{
		Measurements=getMeasurements(robot);
		double sumOfWeights=0;
		for(int j=0;j<numOfParticles;j++)
		{		
			double weight=getWeight(Particle[j],Measurements);
//			System.out.printf("Weight=%f\n", weight);
			sumOfWeights+=weight;
			AccWeight[j]=sumOfWeights;
		
		}
//		System.out.printf("%f\n",sumOfWeights);
		Resample(sumOfWeights);
	}

	public void drawMean(Graphics2D g2d)			//Draws the weighted mean estimate of the particle filter estimate
	{
		g2d.setColor(Color.BLUE);
		double newX=0;
		double newY=0;
		double newHeading=0;
		double totalWeight=0;
		for(int i=0;i<numOfParticles;i++)
		{
			newX+=Weight[i]*Particle[i].X;
			newY+=Weight[i]*Particle[i].Y;
			newHeading+=Weight[i]*0.01*Particle[i].Heading;				//Modified to make the heading change less sensitive
			totalWeight+=Weight[i];
		}
		newX/=totalWeight;
		newY/=totalWeight;
		newHeading/=totalWeight;
		g2d.fillOval((int)newX-5,(int)newY-5,10,10);
		double cos=newX+Math.cos(newHeading)*10;
		double sin=newY+Math.sin(newHeading)*10;
		g2d.drawLine((int)newX,(int)newY,(int)cos,(int)sin);
	}

	public double[] getMeasurements(Pose pose)		
	{
		double[] measurements = new double[SensorDirections.length];
		for(int i = 0; i < SensorDirections.length; i++)
		{
			double heading = pose.Heading + SensorDirections[i];
			double realDistanceToWall = getClosestWallDistance(pose.X,pose.Y, heading);
			measurements[i] = getBeamNoisy(realDistanceToWall);	 
		}
		return measurements;
	}

	public double getBeamNoisy(double realDistance)														
	{
		if(realDistance>maxRange) {return maxRange;}
		return realDistance+random.nextGaussian()*sensorSigma;
	}

	public double getClosestWallDistance(double x, double y, double heading)
	{	
		double closestDistance=9999;
		double xOne, yOne, xTwo, yTwo;
		for(int i=0; i<xPoints.length; i++)
		{
			xOne=xPoints[i];	
			yOne=yPoints[i];
			xTwo=xPoints[(i+1)%numOfSides];
			yTwo=yPoints[(i+1)%numOfSides];
			double distance=intersectionDistance(x,y,heading,xOne,yOne,xTwo,yTwo);
			if(distance<0) {continue;}
			if(distance<closestDistance){closestDistance=distance;}
		}
		return closestDistance;
	}

	public double intersectionDistance(double rayX, double rayY, double heading, double pt1X, double pt1Y, double pt2X, double pt2Y)	//Ray Trace
	{
		double cos=Math.cos(heading);	
		double sin=Math.sin(heading);
		double deltaX=pt2X-pt1X;
		double deltaY=pt2Y-pt1Y;

		//Checks if lines are parallel
		if(Math.abs(deltaY*cos-deltaX*sin)<0.01) return -1;

		double s=(deltaY*(pt1X-rayX)-deltaX*(pt1Y-rayY))/(deltaY*cos-deltaX*sin);
		double t=(sin*(pt1X-rayX)-cos*(pt1Y-rayY))/(deltaY*cos-deltaX*sin);
		if(s<0) {return -1;}
		if(t<0||t>1) {return -1;}
		return s;
	}

	public double getProbability(double measuredDistance, double realDistance, double deltaR)	//Beam Model ----> Probability
	{
		if(realDistance>maxRange)	{return 1-cdfNormal(maxRange, maxRange, measurementVariance);}	
		double lower=cdfNormal(measuredDistance-deltaR, realDistance, measurementVariance);
		double upper=cdfNormal(measuredDistance+deltaR, realDistance, measurementVariance);
		return upper-lower;	
	}
	
	private double ErrorFunction(double x)		//For approximating the cdf of a Normal Distribution
	{
		double xSquared=x*x;
		double a = 8 * (Math.PI - 3) / (3 * Math.PI * (4 - Math.PI));
		return Math.signum(x)*Math.sqrt(1-Math.exp(-xSquared*(4/Math.PI+a*xSquared)/(1+a*xSquared)));
	}
	
	private double cdfNormal(double x, double mean, double variance)
	{
		return 0.5*(1+ErrorFunction((x-mean)/(Math.sqrt(variance*2))));
	}

	public void Resample(double sumOfWeights)
	{
		for (int i=0;i<numOfParticles;i++)
		{
			double sample =random.nextDouble() * sumOfWeights;
			tempParticle[i] = DrawParticle(sample);
		}
		swap = Particle;
		Particle = tempParticle;
		tempParticle = swap;
	}

	private double getWeight(Pose pose, double[] measurements)	//Sensor Model---> Calculates the Weight
	{
		double weight = 1.0;
		for (int i = 0; i < SensorDirections.length; i++)
		{
			double heading = pose.Heading+SensorDirections[i];
			double distanceToWall =getClosestWallDistance(pose.X, pose.Y, heading);
			weight *= getProbability(measurements[i], distanceToWall, deltaR);
		}
		return weight;
	}
	
	private Pose DrawParticle(double sample)
	{
		int lowIndex = 0;
		int highIndex = numOfParticles;
		while (lowIndex <= highIndex)
		{
			int midNum = (lowIndex + highIndex) / 2;
			double rangeLower = midNum - 1 < 0 ? 0 : AccWeight[midNum - 1];
			double rangeUpper = AccWeight[midNum];

			if (sample < rangeLower)
			{highIndex = midNum - 1;}

			else if (sample>=rangeUpper)
			{lowIndex = midNum + 1;}

			else if (sample>=rangeLower && sample<rangeUpper)
			{return Particle[midNum];}
		}
		System.out.printf("Something is wrong\n");
		Pose p=new Pose(-1,-1,-1);
		p=CreateRandomPose();
		return p;
	}	

	public void moveTo(int x, int y)
	{
		double R=Math.sqrt((robot.Y-y)*(robot.Y-y)+(robot.X-x)*(robot.X-x));
		double newHeading=Math.atan((robot.Y-y)/(robot.X-x));
		System.out.printf("New Heading=%f\n", Math.toDegrees(newHeading));
		if(x<robot.X){newHeading+=Math.PI;}
		double omega=newHeading-robot.Heading;
		double V=R;
		Velocity velo=new Velocity(R,omega);
		double deltaT=1;
		robot=MoveExact(robot,velo,deltaT);	
		for(int i=0;i<numOfParticles;i++)
		{
			Particle[i]=MoveNoisyNew(Particle[i],velo,deltaT);
			if(isLocal==true)
			{
				if(Math.sqrt((Particle[i].X-robot.X)*(Particle[i].X-robot.X)+(Particle[i].Y-robot.Y)*(Particle[i].Y-robot.Y))>250)
				{Particle[i]=robot;}
			}
			else
			{
				if(borders.contains(Particle[i].X, Particle[i].Y)==false)
				{
					Particle[i]=CreateRandomPose();
				}
			}
		}			
	}
		
	public Pose CreateRandomAroundRobot(Pose Particle)
	{
		double newX=robot.X+random.nextGaussian()*maxRange;
		double newY=robot.Y+random.nextGaussian()*maxRange;
		return new Pose(newX, newY,Particle.Heading);	
	}
		
	public Pose MoveExact(Pose Particle, Velocity velocity, double deltaT)
	{
		double newHeading=Particle.Heading+velocity.Omega*deltaT;
		double newX=Particle.X+velocity.V*Math.cos(newHeading)*deltaT;
		double newY=Particle.Y+velocity.V*Math.sin(newHeading)*deltaT;
		return new Pose(newX, newY, newHeading);
	}

	public Pose MoveNoisyNew(Pose Particle, Velocity velocity, double deltaT)	
	{
		double newHeading=Particle.Heading+velocity.Omega*deltaT+random.nextGaussian()*sigmaTheta;
		double R=velocity.V*deltaT+random.nextGaussian()*sigmaR;		
		double X=Particle.X+R*Math.cos(newHeading);
		double Y=Particle.Y+R*Math.sin(newHeading);
		double a=sigmaR*Math.cos(newHeading)*Math.cos(newHeading)+sigmaTheta*Math.sin(newHeading)*Math.sin(newHeading);
		double b=R*(sigmaTheta-sigmaR)*Math.cos(newHeading)*Math.sin(newHeading);
		double d=R*R*(sigmaR*Math.sin(newHeading)*Math.sin(newHeading)+sigmaTheta*Math.cos(newHeading)*Math.cos(newHeading));
		double sigma1=Math.sqrt(a);
		double sigma2=Math.sqrt(d);
		double rho=b/(sigma1*sigma2);
		double z1=random.nextGaussian();
		double z2=random.nextGaussian();
		double newX=X+sigma1*z1;
		double newY=Y+sigma2*(rho*z1+Math.sqrt(1-rho*rho)*z2);
		return new Pose(newX, newY, newHeading);
	}

	public void paint(Graphics g)
	{
		super.paint(g);
		Graphics2D g2d=(Graphics2D)g;
		g2d.drawPolygon(borders);
		drawParticles(g2d);
		drawRobot(g2d);
	}	
	
	public void drawRobot(Graphics2D g2d)
	{
		g2d.setColor(Color.RED);
		g2d.fillOval((int)robot.X-5,(int)robot.Y-5,10,10);
		double cos=robot.X+Math.cos(robot.Heading)*maxRange;	
		double sin=robot.Y+Math.sin(robot.Heading)*maxRange;
		g2d.drawLine((int)robot.X,(int)robot.Y,(int)cos,(int)sin);
		g2d.drawOval((int)(robot.X-maxRange),(int)(robot.Y-maxRange),(int)maxRange*2,(int)maxRange*2);
	}

	public void drawParticles(Graphics2D g2d)
	{
		g2d.setColor(Color.BLACK);
		for(int i=0;i<numOfParticles;i++)
		{
			g2d.fillOval((int)Particle[i].X-1,(int)Particle[i].Y-1,(int)2,(int)2);		
		}
	}
	
	//public static void main(String[] args)
//	{
//		JFrame frame = new JFrame("Monte Carlo Localization");	
//		map Map=new map();
//		frame.add(Map);
//		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		frame.setSize(500,540);
//		frame.setVisible(true);
//	}
}
