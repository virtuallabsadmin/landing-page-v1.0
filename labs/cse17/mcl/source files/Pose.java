public class Pose
{
	public double X;
	public double Y;
	public double Heading;
	
	public Pose(double x, double y, double heading)
	{
		X=x;
		Y=y;
		Heading=heading;
	}
	
	public Pose(Pose pose)
	{
		this.X=pose.X;
		this.Y=pose.Y;
		this.Heading=pose.Heading;
	}
		
	public double getX()
	{return X;}
	
	public double getY()
	{return Y;}

	public double getHeading()
	{return Heading;}

	public void setX(int x)
	{X=x;}
	
	public void setY(int y)
	{Y=y;}
	
	public void setHeading(double heading)
	{Heading=heading;}
}


