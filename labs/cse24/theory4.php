<html>
<body>
<h4>Steps for building FSTs for morphological analyzer and generator</h4>
<br>
<ol>
<li> Construct an FST from Delete-Add table for each paradigm. The add string should be
the input label, and the delete string and features should be output labels.<br>

Example:<br>
<img src="./morph_images/laDakA-addDel.fsm.jpg" />
<li> Construct an FST from paradigms table for each paradigm. The input labels and the
output labels will be the same as the stems of the root and word form are the same.<br>

Example:<br>
<div style="margin-left:-20px;">
<img src="./morph_images/laDakA-paradigm.fsm.jpg" width="450px"/>
</div>
<li> Concatenate Add-Delete FST and Paradigm FST of each paradigm to get FSTs 
representing the morphological analyzer for those paradigms.<br>
Example:<br>
<img src="./morph_images/laDakA.fsm.jpg" style="margin-left:-20px;" height="200px" width="450px"/>
<li> Take a Union of the FSTs (morphological analyzers) for each paradigm to get one
FST that represents the morphological analyzer for the language.

<li> The morphological generator can be obtained by taking an inverse of the 
FST representing morphological analyzer.<br>

Example:<br>
<img src="./morph_images/laDakA-inv.fsm.jpg" style="margin-left:-20px;" height="200px" width="450px"/>
</ol>
<br>
<hr>
</body>
</html>
