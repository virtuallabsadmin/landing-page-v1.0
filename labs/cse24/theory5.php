<html>
<body>
<h4>Steps for analyzing an input word</h4><br>
<ol>
<li> Build a single-path FSA for the input word.<br>
Example:<br>
<img src="./morph_images/input-word.fsm.jpg" width="450px" style="margin-left:-20px;"/>

<li> Compose this FSA with FST representing morphological analyzer. Then, project the output
labels and remove epsilon symbols to get an FSA whose paths represent all possible analysis.<br>
<img src="./morph_images/analysis.jpg"  width="450px" style="margin-left:-20px;"/>
<li> Print all the paths of the resulting FSA to get all possible analysis.<br>
Example Output: laDake ==&gt; laDakA+sg+o,laDakA+pl+d 
</ol>
<br>
<br>
<hr>
</body>
</html>
