<html>
<head>
</head>

<title>Morph Experiments</title>
<body>

<h3>Encoding morphology of a root word</h3><br>

Here, we shall see how to represent the morphology of a 'root' word.
Encoding such information allows the analysis of word forms associated
with other 'root' words whose morphology is similar to the 'root' word
in consideration.<br><br>

Consider the example of a root word 'लड़का '. The steps to be followed
to encode the morphology of 'लड़का ' are,
<ol>
	<li>Enumerate all the word forms of 'लड़का (laDakA)'.
	<ul>
		<li> लड़का (laDakA)
		<li> लड़के (laDake)
		<li> लड़कों (laDakoM)
		<li> लड़की (laDakI)
		<li> लड़कियाँ (laDakiyAM)
		<li> लड़कियों  (laDakiyoM)
	</ul><br>
	<li>Associate these word forms with feature combinations. Hindi 
	nouns would have three primary features associated with them gender, 
	number, case.<br><br>
	<table border=1>
	<tr><td><b>Word</b></td><td><b>Root</b></td><td><b>Gender</b></td><td><b>Number</b></td><td><b>Case</b></td></tr>
	<tr> <td>लड़का </td><td>लड़का</td><td>masculine</td><td>singular</td><td>direct</td></tr>
	<tr> <td>लड़के</td><td>लड़का</td><td>masculine</td><td>singular</td><td>oblique</td></tr>
	<tr> <td>लड़के</td><td>लड़का</td><td>masculine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>लड़कों</td><td>लड़का</td><td>masculine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>लड़की </td><td>लड़का</td><td>feminine</td><td>singular</td><td>direct</td></tr>
	<tr> <td>लड़की </td><td>लड़का</td><td>feminine</td><td>singular</td><td>oblique</td></tr>
	<tr> <td>लड़कियाँ</td><td>लड़का</td><td>feminine</td><td>singular</td><td>direct</td></tr>
	<tr> <td>लड़कियों </td><td>लड़का</td><td>feminine</td><td>singular</td><td>direct</td></tr>
	</table>
	<br>
	<ul>
            <li> Oblique case denotes the usage of a word where it has been followed by a 
		postposition. For example, in the sentence 'लड़के ने कहा की ....', 'लड़के' has
		an oblique case.
	</ul>
	<br>
    	<li> From the table above, delete-add rules are extracted. These rules are used to
	analyze a new word whose root behaves similar to 'लड़का '. For computing the delete-add rules,
	first compute the common stem from all the  word forms. In this example, the common stem is
	'लड़क '. Now, subtract this stem from all the words in the table above to get the delete-add
	rules given below.</li><br>
	<table border=1>
	<tr><td><b>Delete</b></td><td><b>Add&nbsp;</b></td><td><b>Gender</b></td><td><b>Number</b></td><td><b>Case</b></td></tr>
	<tr> <td>आ </td><td>आ</td><td>masculine</td><td>singular</td><td>direct</td></tr>
	<tr> <td>ऐ</td><td>आ</td><td>masculine</td><td>singular</td><td>oblique</td></tr>
	<tr> <td>ऐ</td><td>आ</td><td>masculine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>ओं</td><td>आ</td><td>masculine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>ई</td><td>आ</td><td>feminine</td><td>singular</td><td>direct</td></tr>
	<tr> <td>ई </td><td>आ</td><td>feminine</td><td>singular</td><td>oblique</td></tr>
	<tr> <td>इयाँ </td><td>आ</td><td>feminine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>इयों  </td><td>आ</td><td>feminine</td><td>plural</td><td>oblique</td></tr>
	</table>

	<br><br>Here is the table if a Roman script was used.<br><br>

	<table border=1>
	<tr><td><b>Delete</b></td><td><b>Add&nbsp;</b></td><td><b>Gender</b></td><td><b>Number</b></td><td><b>Case</b></td></tr>
	<tr> <td>A </td><td>A</td><td>masculine</td><td>singular</td><td>direct</td></tr>
	<tr> <td>e</td><td>A</td><td>masculine</td><td>singular</td><td>oblique</td></tr>
	<tr> <td>e</td><td>A</td><td>masculine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>oM</td><td>A</td><td>masculine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>I</td><td>A</td><td>feminine</td><td>singular</td><td>direct</td></tr>
	<tr> <td>I </td><td>A</td><td>feminine</td><td>singular</td><td>oblique</td></tr>
	<tr> <td>iyAM </td><td>A</td><td>feminine</td><td>plural</td><td>direct</td></tr>
	<tr> <td>iyoM  </td><td>A</td><td>feminine</td><td>plural</td><td>oblique</td></tr>
	</table>
	<br>
	<br>
	<li> The above tables can be used to analyze words (whose roots behave similar to लड़का  'laDakA').
		<ol>
			<li> The suffix of the word is matched with the delete column	
			<li> The entry in the add column is appended to get the root word. Other features
			are the entries in the remaining columns.
			<li> Exercise: Compute the features of the words 'घोड़ियाँ ' (GoDe), 'चरखे ' (CaraKe) from the above table.
		</ol>
</ol><br>
<hr>
</body>
</html>
