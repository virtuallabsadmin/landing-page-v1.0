<html>
<body>
<b><u>STEP1: </u></b>Select the language.<br>
<b><u>OUTPUT: </u></b>Another drop down to select script will appear in right. <br><br>
<b><u>STEP2: </u></b>Select the script.<br>
<b><u>OUTPUT: </u></b>Drop down to select root will appear below it along with other features.<br><br>
<b><u>STEP3: </u></b>Select the root from the drop down menu.<br>
<b><u>STEP4: </u></b>Select other features for the root i.e. Gender, Number, Reference, Tense.<br>
<b><u>STEP5: </u></b>Click on Form Word!! to generate the word.<br>
<br>
<b><u>OUTPUT: </u></b>The word will appear on top of previously generated words.
<br><br>
<hr>
</body>
</html>
