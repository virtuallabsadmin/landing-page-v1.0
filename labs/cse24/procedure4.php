<html>
<body>
<b><u>STEP1: </u></b>Fill the add-delete table for the given features.<br>
<b><u>STEP2: </u></b>Fill the paradigms table.<br>
<b><u>STEP3: </u></b>Press <button>generate FSM</button> to build the Analyzer.<br>
<b><u>OUTPUT1: </u>add-delete FSM will appear</b>. <br>
<b><u>OUTPUT2: </u>paradigm FSM will appear</b>. <br>
<b><u>OUTPUT3: </u>analyzer FSM will appear</b>. <br><br>
<hr>
</body>
</html>
