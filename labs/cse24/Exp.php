<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />  
<script src="jquery.js"  type="text/javascript" ></script>
	<script type="text/javascript" src="./jquery.fancybox-1.3.1/fancybox/jquery.mousewheel-3.0.2.pack.js"></script>
	<script type="text/javascript" src="./jquery.fancybox-1.3.1/fancybox/jquery.fancybox-1.3.1.js"></script>
	<link rel="stylesheet" type="text/css" href="./jquery.fancybox-1.3.1/fancybox/jquery.fancybox-1.3.1.css" media="screen" />
<script type="text/javascript">
	$(document).ready(function() {
		$("a#example1").fancybox({
			'titleShow'         : false
			});

		});
</script>

<script type="text/javascript">
var state="hidden";
function hide(layerId){
	var elem,vis;
	if(document.getElementById){
		elem=document.getElementByIdO(layerId);
	}
	else if(document.all){
		elem=document.all[layerId];
	}
	else if(document.layers){
		elem=document.layers[layerId];
	}
	vis=elem.styledisplay="none";

}	
function show(layerId){
//	hideAll();
	$('.divClass').fadeOut(100);
	$('#'+layerId).fadeIn(100);
//	document.getElementById(layerId).style.display="block";
}
/*function hideAll(){
	var myDiv = new Array();
	$('.divClass').fadeOut(200);
}*/
</script>
</head>
<body>


<header id="site_head">
<title>NLP</title>
		<div class="header_cont"> <!-- 960px header-->
			<!-- LOGO of the company-->
			<div id="logo">
			<h1> <a href="index.php">Language Technology Research Centre. </a> </h1>
            
			</div>
	  </div>
			<!-- Menu -->
	</header>


<script type="text/javascript">
$('document').ready(function (){

$('sidelink').click(function (){
$('.divs').css('display','none');
ids=this.id;
alert(ids);
$('#div'+ids).fadeIn(500);
	
});


});
</script>

<div id="wrap">
<div id="header">
<?php
$E=$_GET["Exp"];
$sel=$_GET["sel"];
$temp="Experiment ".$E;
echo $temp;

$title[1]="Understanding analysis of words"; 
$title[2]="Understanding generation of words"; 
$title[3]="Encoding morphological information"; 
$title[4]="Creating morphological analyzer"; 
$title[5]="Analysing word using FSM"; 
$title[6]="Generating word using FSM"; 
$title[7]="Word Converter"; 


?>
<br><br><hr>

<a onclick="show('introduction');" href="#" >Introduction</a>&nbsp;&nbsp;&nbsp;
<a onclick="show('theory');" href="#" >Theory</a>&nbsp;&nbsp;&nbsp;
<a onclick="show('objective');" href="#" >Objective</a>&nbsp;&nbsp;&nbsp;
<a onclick="show('procedure');" href="#" >Procedure</a>&nbsp;&nbsp;&nbsp;
<a onclick="show('experiment');" href="#" >Experiment</a>&nbsp;&nbsp;&nbsp;
<a onclick="show('quiz');" href="#" >Quiz</a>&nbsp;&nbsp;&nbsp;
<a onclick="show('reference');" href="#" >Reference</a>&nbsp;&nbsp;&nbsp;
<hr>

</div>
<div id="content">
<div class="menu">
<!--the right most-->
</div>






<div id="introduction" class='divClass' ">
<?php
echo "<h2 align=\"center\">".$title[$E]."</h2><br>"; 
echo "<h3 align='center'> Introduction</h3><br>";
$str="introduction".$E.".php";
include($str);
?>
</div>

<div id="theory" class='divClass' style="display:none;">
<?php
echo "<h2 align=\"center\">".$title[$E]."</h2><br>"; 
echo "<h3 align='center'> Theory</h3><br>";
$str="theory".$E.".php";
include($str);
?>
</div>
<div id="objective" class='divClass' style="display:none;">
<?php
echo "<h2 align=\"center\">".$title[$E]."</h2><br>"; 
echo "<h3 align='center'> Objective</h3><br>";
$str="objective".$E.".php";
include($str);
?>
</div>
<div id="procedure" class='divClass' style="display:none;">
<?php
echo "<h2 align=\"center\">".$title[$E]."</h2><br>"; 
echo "<h3 align='center'> Procedure</h3><br>";
$str="procedure".$E.".php";
include($str);
?>
</div>
<div id="experiment" class='divClass' style="display:none;">
<?php
echo "<h2 align=\"center\">".$title[$E]."</h2><br>"; 
echo "<h3 align='center'> Experiment</h3><br>";
$str="experiment".$E.".php";
include($str);
?>
</div>





<div id="quiz" class='divClass' style="display:none;">
<?php
echo "<h2 align=\"center\">".$title[$E]."</h2><br>"; 
echo "<h3 align='center'>Quiz</h3><br>";
$str="quiz".$E.".php";
include($str);
?>

</div>


<div id="reference" class='divClass' style="display:none;">
<?php
echo "<h2 align=\"center\">".$title[$E]."</h2><br>"; 
echo "<h3 align='center'>Reference</h3><br>";
$str="reference".$E.".php";
include($str);
?>






</div>


<div class='cleared'></div>
<?php
if($E==2){
}
if($E==3){
}
?>
</div>

<div id="bottom" style="padding-top:20px; padding-left:5px;">
<?
//include('footer.php');
?>

</div>
</div>
</body>
</html>
