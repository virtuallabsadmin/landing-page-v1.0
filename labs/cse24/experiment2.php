<html>
<head>
<script type="text/javascript">
var root="";
var gender="";
var num="";
var ref="";
var tense="";
var word=new Array();
var count=0;
var pos="";
var person="";
function setRoot(w,p){
	root=w;
	pos=p;
	$('#option').load('exp2_optload.php?pos='+p+'&root='+w);
}
function setGender(g){
	if(g=='m'){
		gender='male';
		document.getElementById('female').style.backgroundColor="#003300";
		document.getElementById('male').style.backgroundColor="red";
	}
	if(g=='f'){
		gender='female';
		document.getElementById('male').style.backgroundColor="#003300";
		document.getElementById('female').style.backgroundColor="red";
	}
}
function setNumber(n){
	if(n=='s'){
		num='singular';
		document.getElementById('plural').style.backgroundColor="#003300";
		document.getElementById('singular').style.backgroundColor="red";
	}
	if(n=='p'){
		num='plural';
		document.getElementById('singular').style.backgroundColor="#003300";
		document.getElementById('plural').style.backgroundColor="red";
	}
}
function setRef(r){
	if(r=='d'){
		ref='direct';
		document.getElementById('oblique').style.backgroundColor="#003300";
		document.getElementById('direct').style.backgroundColor="red";
	}
	if(r=='o'){
		ref='oblique';
		document.getElementById('direct').style.backgroundColor="#003300";
		document.getElementById('oblique').style.backgroundColor="red";
	}
}
function setTense(t){
	if(t=='spa'){
		tense='simple-past';
		document.getElementById('future').style.backgroundColor="#003300";
		document.getElementById('present').style.backgroundColor="#003300";
		document.getElementById('past').style.backgroundColor="red";
	}
	else if(t=='spr'){
		tense='simple-present';
		document.getElementById('future').style.backgroundColor="#003300";
		document.getElementById('past').style.backgroundColor="#003300";
		document.getElementById('present').style.backgroundColor="red";
	}
	else if(t=='sf'){
		tense='simple-future';
		document.getElementById('present').style.backgroundColor="#003300";
		document.getElementById('past').style.backgroundColor="#003300";
		document.getElementById('future').style.backgroundColor="red";
	}
}
function setPerson(p){
	if(p=="f"){
		person='first';
		document.getElementById('second').style.backgroundColor="#003300";
		document.getElementById('third').style.backgroundColor="#003300";
		document.getElementById('first').style.backgroundColor="red";
	}
	else if(p=="s"){
		person='second';
		document.getElementById('first').style.backgroundColor="#003300";
		document.getElementById('third').style.backgroundColor="#003300";
		document.getElementById('second').style.backgroundColor="red";
	}
	else if(p=="t"){
		person='third';
		document.getElementById('second').style.backgroundColor="#003300";
		document.getElementById('first').style.backgroundColor="#003300";
		document.getElementById('third').style.backgroundColor="red";
	}
}
function getOption(lang,scr){
	document.getElementById("screrr").innerHTML="";
	document.getElementById("option").innerHTML="";
	//document.getElementById('words').innerHTML="";
	if(scr=="null"){
		document.getElementById("screrr").innerHTML="select script";
		return;
	}
	$('#checkoption').load('exp2_opt.php?lang='+lang+'&script='+scr);
}
function setValue(lang){
	document.getElementById('langerr').innerHTML="";
	document.getElementById('script').innerHTML="";
	document.getElementById('option').innerHTML="";
	//document.getElementById('words').innerHTML="";
	scr="<select name='script'>";
	scr=scr+"<option onclick='getOption(\"na\",\"null\");'>Select Script</option>";
	
	if(lang=='null'){
		document.getElementById('langerr').innerHTML="select languange";
		return;
	}
	else if(lang=='en'){
		scr=scr+"<option onclick='getOption(\"en\",\"roman\");'>Roman</option>";
		scr=scr+"</select>";
		document.getElementById('script').innerHTML=scr;
	}
	else if(lang=='hi'){
		scr=scr+"<option onclick='getOption(\"hi\",\"devanagiri\");'>Devanagiri</option>";
		scr=scr+"<option onclick='getOption(\"hi\",\"roman\");'>Roman</option>";
		scr=scr+"</select>";
		document.getElementById('script').innerHTML=scr;
	}
}
function contains(w){
	for(i=0;i<count;i++){
		if(w==word[i]){
			return true;
		}
	}
	return false;
}
function getWord(){
	if(root==""){
		alert("select root");
		return;
	}	
	if(gender==""){
		alert("select gender");
		return;
	}	
	if(num==""){
		alert("select number");
		return;
	}
	if(pos=="noun"){	
		if(ref==""){
			alert("select reference");
			return;
		}	
	}
	if(pos=="verb"){	
		if(tense==""){
			alert("select tense");
			return;
		}	
		if(person==""){
			alert("select person");
			return;
		}	
	}
	xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4){
			temp=xmlhttp.responseText;
			if(temp.length!=1 && !contains(temp)){
				word[count]=temp;
				count=count+1;
			}
			p="<table>";
			for(i=count-1;i>=0;i--){
				if(i==count-1){
					p=p+"<tr bgcolor='green'>"+ word[i] + "</tr>";
				}
				else{
					p=p+"<tr>"+word[i]+"</tr>";
				}
			}
			p=p+"</table>";
			document.getElementById('words').innerHTML=p;
		}
	}
	if(pos=="noun"){
		xmlhttp.open("GET","exp2.php?r="+root+"&g="+gender+"&n="+num+"&ref="+ref+"&pos="+pos,true);
	}
	else if(pos=="verb"){
		xmlhttp.open("GET","exp2.php?r="+root+"&g="+gender+"&n="+num+"&tense="+tense+"&person="+person+"&pos="+pos,true);
	}
	xmlhttp.send();
	xmlhttp.send();
}

</script>
</head>

<body>

<table width="100%">
<tr>
<td>
<select name="lang">
<option selected="selected" onclick="setValue('null');">Select language</option>
<option onclick="setValue('en');">English</option>
<option onclick="setValue('hi');">Hindi</option>
</select>
</td>
<td>
<div id="script"></div>
<tr>
<td><div id="langerr"></div></td>
<td><div id="screrr"></div></td>
</tr>
</td></tr>
</table>
<br>
<div id="checkoption" style="display:hidden"></div>
<div id="option"></div>
<br>
<div align="center" style="padding-right:50px;font-size:16px;">
<div id="words"></div>
</div>
</body>
</html>
