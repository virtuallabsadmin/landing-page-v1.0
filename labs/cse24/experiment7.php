<style>
div.wordconv
{
padding: 10px;
	 font-size: 16px;
	 text-align: center;

}
div.wordconv label
{
padding: 0px 4px;
	 font-weight: bold;
}

div.wordconv input
{
margin: 0 4px;
padding: 4px;
}
.res
{
padding: 5px;
color: #111;
width: 90%;
margin: 5px auto;
       font-size: 16px;
border: 1px solid #e5e5e5;
}
.res h3
{
margin: 5px 0px;
color: #660000;
}
.res table
{
border: 1px solid #660000;
	border-collapse: collapse;
	margin-left: 10px;
	margin-bottom: 10px;
}
.res table th
{
text-transform: capitalize;
background: #660000;
padding: 4px;
color: #fff;
}

.res table td
{

border-bottom: 1px solid #660000;
text-align: center;
}


.res table td
{
padding: 5px;
}
.res strong.last
{
color: #660000;
       font-size: 17px;
}
</style>
<script>
$("document").ready(function () {
		$(".iconv").click(function () {

			word = $(".iword").val();
			if(word == "")
			{
			alert("Please enter word");
			return;
			}

			lang = $(".lang").val();
			url="getConversion.php";

			$.post(url, {word: word,
				lang: lang
				}, function (data)
				{

				$(".res").html(data);
				});



		});
});

</script>


<div class="wordconv">
<label>Word</label>
<select class="iword">
<option value="">Select word</option>
<option value="drank">drank</option>
</select>

<label>Select Language</label>
<select class="lang">
<option value="hindi">Hindi</option>
</select>
<br/>
<br/>
<br/>
<input type="submit" value="Convert" class="iconv">
</div>
<div class="res">
</div>
